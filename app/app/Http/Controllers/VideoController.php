<?php

namespace App\Http\Controllers;

use App\Video;
use Illuminate\Http\Request;

class VideoController extends Controller
{

    public function showAll()
    {
        return response()->json(Video::all());
    }

    public function showOne($id)
    {
        return response()->json(Video::find($id));
    }

    public function create(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'url' => 'required'
        ]);

        $video = Video::create($request->all());

        return response()->json($video, 201);
    }

    public function update($id, Request $request)
    {
        $video = Video::findOrFail($id);
        $video->update($request->all());

        return response()->json($video, 200);
    }

    public function delete($id)
    {
        Video::findOrFail($id)->delete();
        return response('Deleted Successfully', 200);
    }

    public function getUrl($vid){
        
    $url= "https://www.youtube.com/channel?id=".$vid;
    $xml_url = simplexml_load_file($url);

    return response() -> json($xml_url);

    }
}