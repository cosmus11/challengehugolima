<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
  $router->get('videos',  ['uses' => 'VideoController@showAll']);

  //$router->get('ytvideos/{id}', ['uses' => 'VideoController@showOne']);

  $router->post('videos', ['uses' => 'VideoController@create']);

  $router->delete('videos/{id}', ['uses' => 'VideoController@delete']);

  $router->put('videos/{id}', ['uses' => 'VideoController@update']);

  $router->get('videos/url/{vid}', ['uses' => 'VideoController@getUrl']);
});